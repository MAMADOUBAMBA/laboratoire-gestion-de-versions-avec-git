# Gestion de versions avec Git

Outil essentiel en contexte de travail collaboratif, le système de gestion de versions permet de régler les problèmes de suivi de la plus récente version d'un fichier, de partage de fichiers entre les membres d'une équipe et de mise en commun des contributions. 

## Édition

2024.01

## Auteur

Vincent Goulet, professeur titulaire, École d'actuariat, Université Laval

